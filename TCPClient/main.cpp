#include <iostream>
#include <string>
#include <fstream>
#include <WinSock2.h>

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 8400

#pragma comment(lib, "ws2_32.lib")

class CTcpClient {
private:
	SOCKET serverSock;
	SOCKADDR_IN serverData;
	std::string m_ipAddress;
	int m_port;

	std::ifstream m_InFile;
	const char* m_FileName;
	long m_numberOfLines;
public:
	CTcpClient(std::string ipAddress, int port);
	~CTcpClient();

	void runServer();

	void initialize();
	void createSocket();
	void connectSocket();
	void changePort();
	void cleanup();

	void sendLength(const std::string &part);
	void sendToServer(const std::string &part);
	bool recvAnswer();

	// FILES
	void openFile(const char* fileName);
	void countFileLength();
	int sendPartByLines();
};

CTcpClient::CTcpClient(std::string ipAddress, int port)
	: m_ipAddress(ipAddress), m_port(port), serverSock(INVALID_SOCKET), serverData({ 0 }) 
{	}

CTcpClient::~CTcpClient() 
{
	closesocket(serverSock);
	cleanup();
}

void CTcpClient::initialize() {
	std::cout << "initialize()..." << std::endl;
	WSAData data;
	if (WSAStartup(MAKEWORD(2, 2), &data) < 0) {
		std::cout << "Failed to init sock lib: " << WSAGetLastError() << std::endl;
		return;
	}
}

void CTcpClient::createSocket() 
{
	std::cout << "createSocket()..." << std::endl;
	serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (serverSock == INVALID_SOCKET) {
		std::cout << "Failed to create a server socket: " << WSAGetLastError() << std::endl;
		return;
	}
	std::cout << "Socket created" << std::endl;
}

void CTcpClient::connectSocket()
{
	std::cout << "connectSocket()..." << std::endl;
	serverData.sin_family = AF_INET;
	serverData.sin_port = htons(m_port);
	serverData.sin_addr.s_addr = inet_addr("127.0.0.1");

	if (connect(serverSock, (sockaddr*)&serverData, sizeof(serverData)) < 0)
	{
		std::cout << "Failed to connect to server socket: " << WSAGetLastError() << std::endl;
		return;
	}
}

void CTcpClient::changePort()
{
	std::cout << "changePort()..." << std::endl;
	m_port += 5;
}

void CTcpClient::sendToServer(const std::string& part)
{
	//std::cout << "sendTest()..." << std::endl;
	send(serverSock, part.c_str(), part.length(), 0);
}

void CTcpClient::cleanup() 
{
	std::cout << "cleanup()..." << std::endl;
	WSACleanup();
}

void CTcpClient::runServer()
{
	// create socket
	initialize();
	createSocket();
	connectSocket();
}

void CTcpClient::sendLength(const std::string& part)
{
	int length = part.length();
	std::string buf = std::to_string(length);
	send(serverSock, buf.c_str(), buf.length(), 0);
	std::cout << "LENGTH: " << length << std::endl;
}

bool CTcpClient::recvAnswer()
{
	char buf[4096] = { 0 };
	recv(serverSock, buf, sizeof(buf), 0);
	std::string answer = buf;
	if (answer == "true")
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CTcpClient::openFile(const char* fileName)
{
	m_FileName = fileName;
	m_InFile.open(m_FileName, std::ios::binary);
}

void CTcpClient::countFileLength()
{
	std::string buf;
	while (std::getline(m_InFile, buf))
	{
		++m_numberOfLines;
	}
	std::cout << "LINES: " << m_numberOfLines << std::endl;

	// reset file iterator
	m_InFile.clear();
	m_InFile.seekg(0);
}

int CTcpClient::sendPartByLines()
{
	std::string buf;

	long fifthPart = m_numberOfLines / 5;
	for (int i = 0; i < fifthPart; ++i)
	{
		std::getline(m_InFile, buf);
		sendLength(buf);
		sendToServer(buf);

		// server answers if the file has been sent correctly
		if (recvAnswer() == false)
		{
			std::cout << "ERROR DURING FILE SENDING!" << std::endl;
			sendToServer("BREAK");
			return -1;
		}
	}
	sendToServer("END");
	static int i = 20;
	std::cout << "SENDED "<< i << "% OF THE FILE - " << std::endl;
	i += 20;
	return 1;
}

int main()
{
	// create socket
	CTcpClient clientSock(SERVER_IP, SERVER_PORT);
	
	// passing file path/name
	clientSock.openFile("InputFile/test.lsv");
	
	// count how many line are in the file, so we know how to split it in five parts
	clientSock.countFileLength();
	for(int i = 0; i < 5; ++i)
	{
		// open/reopen socket
		clientSock.runServer();

		// send fifth part of the file
		if (clientSock.sendPartByLines() == -1)
		{
			return -1;
		}

		// close socket
		clientSock.cleanup();
		// change port +5
		clientSock.changePort();
	}
	return 0;
}